﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyTrack
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
    }
}
