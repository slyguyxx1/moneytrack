﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace MoneyTrack
{
    public class Purchase
    {
        [PrimaryKey,AutoIncrement]

        public string Name { get; set; }
        public int Cost { get; set; }
        public int ID { get; set; }

    }
}
