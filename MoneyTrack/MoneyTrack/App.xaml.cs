﻿using MoneyTrack.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Xamarin.Forms.Xaml;
using Xamarin.Forms;

namespace MoneyTrack
{
	public partial class App : Application
	{
        static PurchaseDataBase database;
        public App ()
		{
			InitializeComponent();

			MainPage = new MoneyTrack.MainPage();

            
        }

        public int PurhcaseAtId { get; set; }

        public static PurchaseDataBase Database
        {
            get
            {
                if (database == null)
                {
                    database = new PurchaseDataBase(DependencyService.Get<IFileHelper>().GetLocalFilePath("TodoSQLite.db3"));
                }
                return database;
            }
        }

        protected override void OnStart ()
		{
            
        }

        protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
