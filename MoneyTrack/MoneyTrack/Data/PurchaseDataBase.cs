﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using SQLite;

namespace MoneyTrack.Data
{
    public class PurchaseDataBase
    {
        readonly SQLiteAsyncConnection database;

        public PurchaseDataBase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<Purchase>().Wait();
        }

        public Task<List<Purchase>> GetItemsAsync()
        {
            return database.Table<Purchase>().ToListAsync();
        }

        public Task<List<Purchase>> GetItemsNotDoneAsync()
        {
            return database.QueryAsync<Purchase>("SELECT * FROM [TodoItem] WHERE [Done] = 0");
        }

        public Task<Purchase> GetItemAsync(int id)
        {
            return database.Table<Purchase>().Where(i => i.ID == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveItemAsync(Purchase item)
        {
            if (item.ID != 0)
            {
                return database.UpdateAsync(item);
            }
            else
            {
                return database.InsertAsync(item);
            }
        }

        public Task<int> DeleteItemAsync(Purchase item)
        {
            return database.DeleteAsync(item);
        }


    }
}
